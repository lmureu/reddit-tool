# Usage
1. [Register](https://www.reddit.com/prefs/apps/) a Reddit API Application
2. Copy `skel/credentials.py` to `credentials.py`
3. Modify the `auth` section in `credentials.py` with your Reddit API secrets
4. Add your user(s) credentials to the `users` section in `credentials.py`
5. Import client.Client in your script (see [examples](#examples))

# Examples

## 1. Save (another) account's posts
```python
from client import Client
from credentials import users

client = Client(users[0])
redditor = "other_username"
with open(f"{redditor}.txt", "w") as f:
    client.store_user_comments(redditor, f, limit=200) # Store 200 comments max
```

## 2. Account Migration

### 2.1. Single destination
```python
from client import Client
from credentials import users


source_user = users[0]
destination_user = users[1]
client = Client(source_user)
client.migrate(Client(destination_user))
```

### 2.2. Multiple destinations
```python
from client import Client
from credentials import users


source_user = users[0]
client = Client(source_user)
client.migrate_multiple(map(Client, users[1::]))
```
