import logging

from praw import Reddit

import credentials

logging.basicConfig(level=logging.INFO)


class Client:
    def __init__(self, user):
        self.reddit = Reddit(**credentials.auth, **user)
        self.logger = logging.getLogger(__name__)

    def get_subreddits(self):
        return [s.display_name for s in self.reddit.user.subreddits()]

    def get_multireddits(self):
        return [
            {
                "name": multi.name,
                "subreddits": [s.display_name for s in multi.subreddits]
            }

            for multi in self.reddit.user.multireddits()
        ]

    def add_multireddits(self, multireddits):
        for multireddit in multireddits:
            name = multireddit["name"]
            subs = multireddit["subreddits"]

            self.logger.info(f"Adding /m/{name} to /u/{self.reddit.user.me()}")
            self.reddit.multireddit.create(name, subs)

    def add_subreddits(self, subreddits):
        self.reddit.subreddit(subreddits[0]).subscribe(subreddits[1:])

    def store_user_comments(self, username, output_file, limit=100):
        for comment in self.reddit.redditor(username).comments.new(limit=limit):
            output_file.write('\x1e')
            output_file.write(comment.body)
            output_file.flush()

    def migrate(self, other: 'Client'):
        self.logger.info(
            f"Migrating {self.reddit.user.me()} to {other.reddit.user.me()}")

        # Migrate subreddits
        other.add_subreddits(self.get_subreddits())

        # Migrate multireddits
        other.add_multireddits(self.get_multireddits())

    def migrate_multiple(self, others: ['Client']):
        for other in others:
            self.migrate(other)
