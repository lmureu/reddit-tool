auth = {
    "client_id": "",  # Reddit API Client ID
    "client_secret": "",  # Reddit API Client Secret
    "user_agent": "",  # A string of your choice representing your client
}

users = [  # A list of users
    {
        "username": "",  # Username
        "password": "",  # Password
        "skip": False  # Should this be skipped while iterating?
    },
    {
        "username": "",  # Username
        "password": "",  # Password
        "skip": True  # Should this be skipped while iterating?
    },
]
